package tech.talk.example.javaapplication;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "characters")
public class Character {
    @Id
    private long id;
    @Column(name = "name_of_character")
    private String name;
    private String phrase;
}
