package tech.talk.example.javaapplication;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CharacterRepository extends CrudRepository<Character, Long> {

    List<Character> findAll();
}
