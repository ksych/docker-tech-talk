package tech.talk.example.javaapplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
@RequestMapping("/")
public class CharacterController {

    @Autowired
    private CharacterService service;

    @GetMapping("characters")
    public String characters(Map<String, Object> model) {
        model.put("characters", service.getAll());
        return "index";
    }

}
