###This repository contains code that I used for live-demo  on Docker tech talk 29.08.17

#####The cases were following:

1.  Build "postgres-with-schema" image
```
cd postges_with_schema/

docker build -t postgres-with-schema:0.1   .

```




2. Run container with db
```
docker run -p 9042:5432 --name characters-postgres -d postgres-with-schema:0.1

```




3. To know if container is running. Following command prints list of all running containers
```
docker ps
```




4. To view information about container
```
docker inspect characters-postgres 

```




5. Build "characters-app" image
```
cd java-application/

docker build -t characters-app:0.1 .
```




6. Run application
```
docker run -p 8080:8080 --name  characters -e DB_HOST=172.17.0.2 -d -v ~/applogs:/usr/src/myapp/logs characters-app:0.1

```



7. Run db and app with one command
```
docker-compose up
```




8. Run cassandra cluster
```
cd cassandra/

docker-compose up --scale nodes=2

```



9. Connect directly to container
```
docker exec -it cassandra_seed_cassandra_1 bash
```