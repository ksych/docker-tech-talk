CREATE TABLE characters (
    id int,
    name_of_character varchar(255),
    phrase varchar(255)
);

INSERT INTO characters VALUES (1, 'Batman', 'i m batman');
INSERT INTO characters VALUES (2, 'Godzilla', 'rrrrr');
INSERT INTO characters VALUES (3, 'Buzz Lightyear', 'To infinity... and beyond!');
INSERT INTO characters VALUES (4, 'Rick', 'Wubba Lubba Dub-Dub');
INSERT INTO characters VALUES (5, 'Sheldon', 'Bazinga!');
